#!/bin/bash
set -eEuo pipefail

source ./btrfs-common.bash

ensure_have_phy

for phy_dev in "${phy_devs[@]}"
do
	# Make partition read only
	sudo blockdev --setro "$phy_dev"
done

# Clean up DM
sudo dmsetup table | \
	while read -r device _
	do
		device=${device%:}
		if [[ "$device" == btrfs-dr-* ]]
		then
			sudo dmsetup remove "$device"
		fi
	done

# Clean up loop devices
losetup | \
	while read -r name _ _ _ _ file _
	do
		if [[ "$file" == "$cow_data_root"/* ]]
		then
			sudo losetup -d "$name"
		fi
	done

# Disable COW on the temporary COW backing store directory (assuming it's on btrfs).
sudo chattr +C "$cow_data_root" || true

# Create COW storage
declare -A part_cow_loops
for phy_dev in "${phy_devs[@]}"
do
	name="$(basename "$phy_dev")"
	cow_data_file="$cow_data_root"/"$name"
	rm -f "$cow_data_file"
	dd if=/dev/zero bs=1T count=0 seek=64 of="$cow_data_file"
	loop=$(sudo losetup --find --show "$cow_data_file")
	part_cow_loops[$phy_dev]=$loop
done

# Create snapshot DM targets
for phy_dev in "${phy_devs[@]}"
do
	name="$(basename "$phy_dev")"
	loop="${part_cow_loops[$phy_dev]}"
	sudo dmsetup create btrfs-dr-"$name" --table "0 $(sudo blockdev --getsz "$phy_dev") snapshot $phy_dev $loop N 8"
done

# Make block devices accessible to qemu running as current user
sudo chown 1000:1000 '/dev/mapper/btrfs-dr-'*

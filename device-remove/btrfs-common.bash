loop_path=/mnt/data2016/tmp/2019-07-05b/disk
cow_data_root=/mnt/data2016/tmp/2019-07-06
#cow_data_root=/tmp/2019-07-06
mountpoint=/mnt/a

sudo true # acquire cookie

function ensure_have_phy() {
	# Attach loop device
	local _ name file
	while read -r name _ _ _ _ file _
	do
		if [[ "$file" == "$loop_path" ]]
		then
			loop_dev=$name
		fi
	done < <(losetup)

	if [[ ! -v loop_dev ]]
	then
		loop_dev=$(sudo losetup -f --show "$loop_path")
	fi

	phy_devs=(
		/dev/disk/by-partuuid/69a75519-1f3c-49a3-a36d-96b2af322413
		/dev/disk/by-partuuid/7056ce22-b5e7-4e24-aa13-58ecc6a6ca82
		"$loop_dev"
	)
}

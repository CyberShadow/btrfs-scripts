#!/bin/bash
set -eEuo pipefail
shopt -s lastpipe

source ./btrfs-common.bash 

ensure_have_phy

opts=ro
for dev in "${phy_devs[@]}"
do
	opts="$opts,device=$dev"
done

sudo mount -o "$opts" "${phy_devs[0]}" "$mountpoint"

#!/bin/bash
set -eEuo pipefail

# args=(
# 	sudo
# #	t
# 	qemu-system-x86_64
# 	-nographic
# 	# -hda /home/vladimir/data/vm/archlinux-btrfs-kernel/disk
# 	-drive 'file=/home/vladimir/data/vm/archlinux-btrfs-kernel/disk,if=virtio'
# 	# -kernel /home/vladimir/work/extern/arch/abs/packages/linux/trunk/src/archlinux-linux/vmlinux
# 	-kernel /boot/vmlinuz-linux
# 	-initrd /boot/initramfs-linux.img
# 	-append 'console=ttyS0'
# ) ; "${args[@]}"


workdir=/mnt/local/tmp/linux-qemu

# shellcheck disable=SC2191
kargs=(
	console=ttyS0
	log_buf_len=64M
	loglevel=7
	nokaslr
	# debug
)

args=(
	qemu-system-x86_64
	-kernel "$workdir"/linux/arch/x86_64/boot/bzImage
	-initrd "$workdir"/initramfs.cpio.gz
	-hda "$workdir"/root.img
	-hdb /dev/mapper/btrfs-dr-loop0
	-hdc /dev/mapper/btrfs-dr-69a75519-1f3c-49a3-a36d-96b2af322413
	-hdd /dev/mapper/btrfs-dr-7056ce22-b5e7-4e24-aa13-58ecc6a6ca82
	-m 4G
	-nographic -append "${kargs[*]}"
	-cpu host
	# -s -S
	-enable-kvm
) ; exec "${args[@]}"

#!/bin/bash
set -eEuo pipefail

workdir=/mnt/local/tmp/linux-qemu

if [[ ! -f "$workdir"/busybox/.built ]]
then
	rm -rf "$workdir"/busybox
	mkdir -p "$workdir"/busybox

	test -d ~/work/extern/busybox || git clone git://git.busybox.net/busybox ~/work/extern/busybox

	echo 'CONFIG_STATIC=y' > "$workdir"/busybox/.config
	( yes '' || true ) | ccache-run make -C ~/work/extern/busybox O="$workdir"/busybox oldconfig
	ccache-run make -C "$workdir"/busybox -j"$(nproc)"
	make -C "$workdir"/busybox install

	touch "$workdir"/busybox/.built
fi

if true
then
	rm -rf "$workdir"/initramfs
	mkdir -p "$workdir"/initramfs

	# if true
	# then
		mkdir -pv "$workdir"/initramfs/{bin,sbin,etc,proc,sys,usr/{bin,sbin},mnt}
		cp -av "$workdir"/busybox/_install/* "$workdir"/initramfs/
	# else
	# 	tar zxf ~/Downloads/2019-07-06/archlinux-bootstrap-2019.06.01-x86_64.tar.gz -C "$workdir"/initramfs
	# 	mv "$workdir"/initramfs/root.x86_64/* "$workdir"/initramfs
	# fi

	test -e boot-script && cp boot-script "$workdir"/initramfs/

	cat <<'EOF' > "$workdir"/initramfs/init
#!/bin/sh

set -x

mount -t proc none /proc
mount -t sysfs none /sys

echo -e "\nBoot took $(cut -d' ' -f1 /proc/uptime) seconds\n"

mknod /dev/sda b 8 0
mount /dev/sda /mnt/

rm -f /mnt/boot-script
if [ -e /boot-script ] ; then
	cp /boot-script /mnt/
	chmod +x /mnt/boot-script
fi

# mount -t proc none /mnt/proc
# mount -t sysfs none /mnt/sys
# exec /usr/sbin/chroot /mnt/ /bin/bash
exec /usr/sbin/chroot /mnt/ /lib/systemd/systemd

exec /bin/sh
EOF

	chmod +x "$workdir"/initramfs/init

(
	cd "$workdir"/initramfs
	find . -print0 \
		| cpio --null -ov --format=newc \
		| gzip -1 \
			   > "$workdir"/initramfs.cpio.gz
)
fi

if false
then
	rm -rf "$workdir"/root
	mkdir "$workdir"/root

	rm -f "$workdir"/root.img
	dd if=/dev/zero of="$workdir"/root.img bs=1G count=0 seek=1

	root_tar=~/Downloads/2019-07-06/archlinux-bootstrap-2019.06.01-x86_64.tar.gz
	WORKDIR="$workdir" TAR="$root_tar" fakeroot bash -s <<'EOF'
		set -xeEuo pipefail
		tar zxvf "$TAR" -C "$WORKDIR"/root
        pacman -r "$WORKDIR"/root/root.x86_64 -Sy --noconfirm --noscriptlet btrfs-progs
		mkfs.ext4 "$WORKDIR"/root.img -d "$WORKDIR"/root/root.x86_64
EOF

fi


if true
then
	if true
	then
		if true
		then
			git -C ~/work/extern/linux reset --hard
			git -C ~/work/extern/linux am --abort || true
			# git -C ~/work/extern/linux checkout d9af0bbade6376c8d5b463272ae4ad1c1573478d # my-btrfs-debug-base-20190810
			# git -C ~/work/extern/linux checkout eec54b57f6d1d85d50e3b94af38eb25c1fd014dd # btrfs-devel/misc-next @ 2019-08-20
			# git -C ~/work/extern/linux checkout eecb23ab61d19c52a5a01f3cfe466749a2c7f297^ # before commit modifying file away from version on which Josef's first patch is based on
			# git -C ~/work/extern/linux checkout 87d39da30f81fa785377d784cdea7723e9ef2b80 # btrfs-devel/misc-next @ 2019-08-24
			git -C ~/work/extern/linux checkout 267d2baa2faf6659a176dd532a8df8318a10b9b1 # btrfs-devel/for-next-20190823 @ 2019-08-24

			patches=(
			# 	1565072673161307 # [PATCH 0/5] Rework eviction space flushing
			# 	1565072673146773 # [PATCH 1/5] btrfs: add a flush step for delayed iputs
			# 	1565072673149581 # [PATCH 2/5] btrfs: unify error handling for ticket flushing
			# 	1565072673156283 # [PATCH 3/5] btrfs: factor out the ticket flush handling
			# 	1565072673039280 # [PATCH 4/5] btrfs: refactor priority_reclaim_metadata_space
			# 	1565072673038401 # [PATCH 5/5] btrfs: introduce an evict flushing state

			#	1565072666843295 # [PATCH] btrfs: add an ioctl to force chunk allocation
			#	1565072642508280 # [PATCH][v2] btrfs: add an ioctl to force chunk allocation
			#	1565072635327985 # [PATCH][v3] btrfs: add a force_chunk_alloc to space_info's sysfs

			#	1565284006069444 # [PATCH 00/15] Migrate the block group code into it's own file
			# 	1565284006108404 # [PATCH 01/15] btrfs: migrate the block group caching code
			# 	1565284006103258 # [PATCH 02/15] btrfs: temporarily export inc_block_group_ro
			# 	1565284005912464 # [PATCH 03/15] btrfs: migrate the block group removal code
			# 	1565284005772459 # [PATCH 04/15] btrfs: export get_alloc_profile
			# 	1565284005637141 # [PATCH 05/15] btrfs: migrate the block group read/creation code
			# 	1565284005524377 # [PATCH 06/15] btrfs: temporarily export btrfs_get_restripe_target
			# 	1565284005510274 # [PATCH 07/15] btrfs: migrate inc/dec_block_group_ro code
			# 	1565284005364329 # [PATCH 08/15] btrfs: migrate the dirty bg writeout code
			# 	1565284005360856 # [PATCH 09/15] btrfs: export block group accounting helpers
			# 	1565284005242269 # [PATCH 10/15] btrfs: migrate the block group space accounting helpers
			# 	1565284005246219 # [PATCH 11/15] btrfs: migrate the chunk allocation code
			# 	1565284005043291 # [PATCH 12/15] btrfs: migrate the alloc_profile helpers
			# 	1565284005048468 # [PATCH 13/15] btrfs: migrate the block group cleanup code
			# 	1565284004796679 # [PATCH 14/15] btrfs: unexport the temporary exported functions
			# 	1565284004886960 # [PATCH 15/15] btrfs: remove comment and leftover cruft

			# 	1565452492896554 # [PATCH 0/7] Rework reserve ticket handling
			# 	1565452492903228 # [PATCH 1/7] btrfs: do not allow reservations if we have pending tickets
			# 	1565452492911523 # [PATCH 2/7] btrfs: roll tracepoint into btrfs_space_info_update helper
			# 	1565452493028825 # [PATCH 3/7] btrfs: add space reservation tracepoint for reserved bytes
			# 	1565452492667446 # [PATCH 4/7] btrfs: rework btrfs_space_info_add_old_bytes
			# 	1565452492684060 # [PATCH 5/7] btrfs: refactor the ticket wakeup code
			# 	1565452492697190 # [PATCH 6/7] btrfs: rework wake_all_tickets
			# 	1565452492772210 # [PATCH 7/7] btrfs: remove orig_bytes from reserve_ticket

			#	1566045935214361 # [PATCH 0/8][v2] Rework reserve ticket handling
				1566045935104280 # [PATCH 1/8] btrfs: do not allow reservations if we have pending tickets
				1566045935106952 # [PATCH 2/8] btrfs: roll tracepoint into btrfs_space_info_update helper
				1566045935107748 # [PATCH 3/8] btrfs: add space reservation tracepoint for reserved bytes
				1566045935109333 # [PATCH 4/8] btrfs: rework btrfs_space_info_add_old_bytes
				1566045935108559 # [PATCH 5/8] btrfs: refactor the ticket wakeup code
				1566045934892581 # [PATCH 6/8] btrfs: rework wake_all_tickets
				1566045934900428 # [PATCH 7/8] btrfs: fix may_commit_transaction to deal with no partial filling
				1566045934910823 # [PATCH 8/8] btrfs: remove orig_bytes from reserve_ticket

			#	1566045934523009 # [PATCH 0/3] Rework the worst case calculations for space reservation
				1566045934524675 # [PATCH 1/3] btrfs: rename the btrfs_calc_*_metadata_size helpers
				1566045934523855 # [PATCH 2/3] btrfs: only reserve metadata_size for inodes
				1566045934525867 # [PATCH 3/3] btrfs: global reserve fallback should use metadata_size

			#	1566045934373782 # [PATCH 0/5] Fix global reserve size and can overcommit
				1566045934379558 # [PATCH 1/5] btrfs: change the minimum global reserve size
				1566045934378684 # [PATCH 2/5] btrfs: always reserve our entire size for the global reserve
				1566045932595105 # [PATCH 3/5] btrfs: use add_old_bytes when updating global reserve
				1566045932667410 # [PATCH 4/5] btrfs: do not account global reserve in can_overcommit
				1566045932676062 # [PATCH 5/5] btrfs: add enospc debug messages for ticket failure
			)
			maildir='/home/vladimir/.thunderbird/ypupxmf3.default/ImapMail/imap.gmail-1.com/[Gmail].sbd/All Mail-1/cur/'
			patches=("${patches[@]/#/$maildir}")
		#	git -C ~/work/extern/linux am "${patches[@]}"
		fi

		rm -rf "$workdir"/linux

		ccache-run make -C ~/work/extern/linux O="$workdir"/linux x86_64_defconfig
		ccache-run make -C ~/work/extern/linux O="$workdir"/linux kvmconfig

		(
			echo 'CONFIG_BTRFS_FS=y'

			echo 'CONFIG_BTRFS_FS_CHECK_INTEGRITY=y'
			echo 'CONFIG_BTRFS_DEBUG=y'
			echo 'CONFIG_BTRFS_ASSERT=y'
			echo 'CONFIG_BTRFS_FS_REF_VERIFY=y'

			echo 'CONFIG_DEBUG_INFO=y'
			echo 'CONFIG_GDB_SCRIPTS=y'
		) >> "$workdir"/linux/.config

		ccache-run make -C ~/work/extern/linux O="$workdir"/linux olddefconfig
	fi

	ccache-run make -C ~/work/extern/linux O="$workdir"/linux -j"$(nproc)"
fi

#!/bin/bash
set -eEuo pipefail

cd "$(dirname "$0")"

pwd=$PWD

git -C ~/work/extern/linux bisect reset
git -C ~/work/extern/linux bisect start
git -C ~/work/extern/linux bisect good eecb23ab61d19c52a5a01f3cfe466749a2c7f297^
git -C ~/work/extern/linux bisect bad  4bf7e388eba1
git -C ~/work/extern/linux bisect run "$PWD"/bisect-run-script.sh

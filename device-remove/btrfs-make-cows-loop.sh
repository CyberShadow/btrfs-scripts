#!/bin/bash
set -eEuo pipefail

while true
do
	if [[ -f .make-cows ]]
	then
		echo 'Making COW images...'
		./btrfs-make-cows.sh
		rm .make-cows
	fi
	echo 'Waiting...'
	sleep 1
done

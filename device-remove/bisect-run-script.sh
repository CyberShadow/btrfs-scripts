#!/bin/bash
set -eEuo pipefail

cd "$(dirname "$0")"

touch .make-cows
while [[ -f .make-cows ]]
do
	echo 'Waiting for COWs...'
	sleep 1
done

if ! ./vm-build.sh
then
	exit 125
fi

fn=bisect-logs/"$(date +%s)".log
# timeout --foreground $((60*90)) \
./vm-boot.sh | tee "$fn"

if grep -qF 'kernel BUG' "$fn"
then
	# String found - kernel bug still occurs
	exit 0 # old
else
	# String not found - bug fixed
	exit 1 # new
fi

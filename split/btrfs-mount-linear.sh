#!/bin/bash
set -eEuo pipefail

# Configuration

source ./config.bash
mountpoint=/mnt/b

# Implementation

sudo true # Acquire cookie

# Clean up DM
sudo dmsetup table | \
	while read -r device _
	do
		device=${device%:}
		if [[ "$device" == btrfs-split-linear-* ]]
		then
			sudo dmsetup remove "$device"
		fi
	done

# Create DM targets
for part in "${parts[@]}"
do
	#dev="${part_devices[$part]}"
	dev=/dev/disk/by-partuuid/"$part"
	sudo dmsetup create btrfs-split-linear-"$part" --table "0 $(sudo blockdev --getsz "$dev") linear $dev 0"
done

# Create mount option string
opts=ro
for part in "${parts[@]}"
do
	opts="$opts,device=/dev/mapper/btrfs-split-linear-$part"
done

sudo mount -o "$opts" "/dev/mapper/btrfs-split-linear-${parts[0]}" "$mountpoint"

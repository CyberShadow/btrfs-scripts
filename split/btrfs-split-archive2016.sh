#!/bin/bash
set -eEuo pipefail

# Configuration

source ./config.bash

# Implementation

sudo true # Acquire cookie

# Resolve partition UUIDs to device names

declare -A part_devices
function scan_devices() {
	local part
	for part in "${parts[@]}"
	do
		# shellcheck disable=SC2024
		sudo blkid -t PARTUUID="$part" > blkid.txt
		if [[ "$(wc -l < blkid.txt)" -ne 1 ]]
		then
			printf 'Found zero or more than one block device for PARTUUID %q!\n' "$part" 1>&2
			exit 1
		fi

		local dev
		dev="$(< blkid.txt cut -d : -f 1)"
		part_devices[$part]=$dev
	done
}
scan_devices

# Ensure source devices are read-only, to prevent accidentally writing to them.

for part in "${parts[@]}"
do
	# Make partition read only
	sudo blockdev --setro "${part_devices[$part]}"
	# Make ATA read-only too, for good measure
	disk=$(printf %s "${part_devices[$part]}" | sed -n 's#^\(/dev/...\)[0-9]*$#\1#p')
	sudo hdparm -r 1 "$disk"
done

# Clean up DM
sudo dmsetup table | \
	while read -r device _
	do
		device=${device%:}
		if [[ "$device" == btrfs-split-* ]]
		then
			sudo dmsetup remove "$device"
		fi
	done

# Clean up loop devices
losetup | \
	while read -r name _ _ _ _ file
	do
		if [[ "$file" == "$cow_data_root"/* ]]
		then
			sudo losetup -d "$name"
		fi
	done

# Disable COW on the temporary COW backing store directory (assuming it's on btrfs).
sudo chattr +C "$cow_data_root"

# Create COW storage
declare -A part_cow_loops
for part in "${parts[@]}"
do
	cow_data_file="$cow_data_root"/"$part"
	rm -f "$cow_data_file"
	dd if=/dev/zero bs=1T count=0 seek=64 of="$cow_data_file"
	loop=$(sudo losetup --find --show "$cow_data_file")
	part_cow_loops[$part]=$loop
done

# Create snapshot DM targets
for part in "${parts[@]}"
do
	dev="${part_devices[$part]}"
	loop="${part_cow_loops[$part]}"
	sudo dmsetup create btrfs-split-"$part" --table "0 $(sudo blockdev --getsz "$dev") snapshot $dev $loop N 8"
done

# Create fake /proc/partitions for libblkid
(
	printf 'major minor  #blocks  name\n\n'
	while read -r major minor sz name
	do
		is_target=false
		for part in "${parts[@]}"
		do
			if [[ "$(readlink /dev/mapper/btrfs-split-"$part")" == ../"$name" ]]
			then
				is_target=true
			fi
		done

		if $is_target
		then
			printf ' %s %s %s %s\n' "$major" "$minor" "$sz" "$name"
		fi
	done < /proc/partitions
) > partitions.txt

# Run btrfstune with fake /proc/partitions
args=(
	sudo
	bwrap
	--dev-bind / /
	--bind partitions.txt /proc/partitions
	btrfstune
	-U "$new_uuid" # or -M (faster, Linux >= 5.0 only)
	/dev/mapper/btrfs-split-"${parts[0]}"
) ; "${args[@]}"

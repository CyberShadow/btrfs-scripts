# Partition UUIDs (PARTUUID) containing filesystem to split
parts=(
	8866d95a-9022-4a64-b03e-faacb642cd8c # /dev/sdc1
	e804507f-324c-4e83-ba53-e0a5be7de701 # /dev/sde1
	19a92349-6170-4eb8-853e-4e1826617ab6 # /dev/sdg1
	a1f378aa-8c96-4ca5-b3ed-28a239d37b9b # /dev/sdj1
)

# Where to place the temporary COW data.
cow_data_root=/mnt/data2016/tmp/2019-06-06

# New UUID. Use uuidgen to generate it.
new_uuid=939881d5-cff0-4395-8fdd-3c23d64da5a5

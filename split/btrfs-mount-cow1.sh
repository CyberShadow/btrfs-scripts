#!/bin/bash
set -eEuo pipefail

# Configuration

source ./config.bash

mountpoint=/mnt/a

# Implementation

sudo true # Acquire cookie

# Create mount option string
opts=rw
for part in "${parts[@]}"
do
	opts="$opts,device=/dev/mapper/btrfs-split-$part"
done

sudo mount -o "$opts" "/dev/mapper/btrfs-split-${parts[0]}" "$mountpoint"

#!/bin/bash
set -eEuo pipefail

# Configuration

source ./config.bash

cow2_data_root="$cow_data_root"/2
cache_root=/tmp/2019-06-06
mountpoint=/mnt/a

# Implementation

sudo true # Acquire cookie

# Clean up DM
sudo dmsetup table | \
	while read -r device _
	do
		device=${device%:}
		if [[ "$device" == btrfs-split-2-cached-* ]]
		then
			sudo dmsetup remove "$device"
		fi
	done

sudo dmsetup table | \
	while read -r device _
	do
		device=${device%:}
		if [[ "$device" == btrfs-split-2-* ]]
		then
			sudo dmsetup remove "$device"
		fi
	done

# Clean up loop devices
losetup | \
	while read -r name _ _ _ _ file
	do
		if [[ "$file" == "$cow2_data_root"/* || "$file" == "$cache_root"/* ]]
		then
			sudo losetup -d "$name"
		fi
	done

# Disable COW on the temporary COW backing store directory (assuming it's on btrfs).
sudo chattr +C "$cow2_data_root"

# Create COW storage
declare -A part_cow_loops
for part in "${parts[@]}"
do
	cow_data_file="$cow2_data_root"/"$part"
	rm -f "$cow_data_file"
	dd if=/dev/zero bs=1T count=0 seek=64 of="$cow_data_file"
	loop=$(sudo losetup --find --show "$cow_data_file")
	part_cow_loops[$part]=$loop
done

# Create snapshot DM targets
for part in "${parts[@]}"
do
	dev=/dev/mapper/btrfs-split-"$part"
	loop="${part_cow_loops[$part]}"
	sudo dmsetup create btrfs-split-2-"$part" --table "0 $(sudo blockdev --getsz "$dev") snapshot $dev $loop N 8"
done

# Create cache storage
declare -A part_cache_data_loops
declare -A part_cache_meta_loops
for part in "${parts[@]}"
do
	cache_data_file="$cache_root"/"$part"-data
	cache_meta_file="$cache_root"/"$part"-meta
	rm -f "$cache_data_file" "$cache_meta_file"
	dd if=/dev/zero bs=1G count=0 seek=1 of="$cache_data_file"
	dd if=/dev/zero bs=1G count=0 seek=1 of="$cache_meta_file"
	loop_data=$(sudo losetup --find --show "$cache_data_file")
	part_cache_data_loops[$part]=$loop_data
	loop_meta=$(sudo losetup --find --show "$cache_meta_file")
	part_cache_meta_loops[$part]=$loop_meta
done

# Create cache DM targets
for part in "${parts[@]}"
do
	dev=/dev/mapper/btrfs-split-2-"$part"
	loop_data="${part_cache_data_loops[$part]}"
	loop_meta="${part_cache_meta_loops[$part]}"
	sudo dmsetup create btrfs-split-2-cached-"$part" --table "0 $(sudo blockdev --getsz "$dev") cache $loop_meta $loop_data $dev 512 0 default 0"
done

# Create mount option string
opts=rw
for part in "${parts[@]}"
do
	opts="$opts,device=/dev/mapper/btrfs-split-2-cached-$part"
done

sudo mount -o "$opts" "/dev/mapper/btrfs-split-2-cached-${parts[0]}" "$mountpoint"

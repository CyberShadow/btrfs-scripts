#!/bin/bash
set -eEuo pipefail

# Configuration

source ./config.bash

mountpoint=/mnt/a

items=(
	'home/@arch*'
	'home/@home*'
	'home/@note4*'
	'home/@ubuntu*'
	'home/@win*'
	'k3/@data*'
	'k3/@home*'
	'k3/@ram*'
	'k3/@root*'
	'@windata'
	'@archive2016'
)

dir=measurements-$(date +%s)
mkdir "$dir"

echo 'Syncing...'
env -C "$mountpoint" sh -c "btrfs subvolume sync ."

echo 'Deleting files...'
env -C "$mountpoint" sh -c "find -mount -type f -print -delete"

btrfs-info "$mountpoint" > "$dir/""$(date +%s)""-initial.txt"

for item in "${items[@]}"
do
	printf '=== %s ===\n' "$item"
	# shellcheck disable=SC2086
	if test -z "$(shopt -s nullglob ; echo "$mountpoint"/$item)"
	then
		printf 'Does not exist, skipping\n'
		continue
	fi

	env -C "$mountpoint" sh -c "btrfs subvolume delete -C $item && btrfs subvolume sync ."
	(
		printf '=== After deleting %s ===\n' "$item"
		btrfs-info "$mountpoint"
	) >> "$dir/$(date +%s).txt"
done

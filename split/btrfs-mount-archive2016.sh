#!/bin/bash
set -eEuo pipefail

exit 1

parts=(
	# last mounted
	8866d95a-9022-4a64-b03e-faacb642cd8c # /dev/sdc1
	e804507f-324c-4e83-ba53-e0a5be7de701 # /dev/sde1
	19a92349-6170-4eb8-853e-4e1826617ab6 # /dev/sdg1
	a1f378aa-8c96-4ca5-b3ed-28a239d37b9b # /dev/sdj1

	# previous
	# 69a75519-1f3c-49a3-a36d-96b2af322413 # /dev/sdd
	# 7056ce22-b5e7-4e24-aa13-58ecc6a6ca82 # /dev/sdf
)


opts=ro
for p in "${parts[@]}"
do
	opts=$opts,device=/dev/disk/by-partuuid/$p
done

sudo mount -o "$opts" /dev/disk/by-partuuid/8866d95a-9022-4a64-b03e-faacb642cd8c /mnt/2016-hdd-8t-raid

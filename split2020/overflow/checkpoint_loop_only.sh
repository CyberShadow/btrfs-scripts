#!/bin/bash
set -eEuo pipefail
set -m
shopt -s lastpipe

################################################################################
# Configuration

source ./config.bash

################################################################################
# Implementation

[[ $EUID == 0 ]]
ulimit -v unlimited

function checkpoint() {
	echo 'Checkpointing...'

	fsfreeze -f "$mountpoint"
	sleep 1

	rm -vrf "$checkpoint_dir"/next
	mkdir "$checkpoint_dir"/next
	clone "$ssd_work" "$checkpoint_dir"/next/ssd-work

	rm -vrf "$checkpoint_dir"/last
	if [[ -e "$checkpoint_dir"/curr ]] ; then mv -v "$checkpoint_dir"/{curr,last} ; fi
	mv -v "$checkpoint_dir"/{next,curr}
	sync -f "$checkpoint_dir"

	fsfreeze -u "$mountpoint"
}

################################################################################
# Action

while true
do
	countdown 3600
	checkpoint
done

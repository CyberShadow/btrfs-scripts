#hdd_root=/mnt/2020-hdd-12t-raid/@data2020/archive2020
ssd_root=/mnt/local/tmp/2020-08-25-overflow
ram_root=/tmp/2020-08-25-overflow

#hdd_work="$hdd_root"/work
ssd_work="$ssd_root"/work

    data_dir="$ssd_work"/data
metadata_dir="$ssd_work"/metadata

  target_dir="$ram_root"/target
upstream_dir="$ram_root"/upstream
checkpoint_dir="$ssd_root"/checkpoint

parts=(
	8866d95a-9022-4a64-b03e-faacb642cd8c # /dev/sdc1
	e804507f-324c-4e83-ba53-e0a5be7de701 # /dev/sde1
	19a92349-6170-4eb8-853e-4e1826617ab6 # /dev/sdg1
	a1f378aa-8c96-4ca5-b3ed-28a239d37b9b # /dev/sdj1
)

mountpoint=/mnt/2016-hdd-8t-raid

function clone() {
	cp -v -a --reflink=always --sparse=auto "$@"
}

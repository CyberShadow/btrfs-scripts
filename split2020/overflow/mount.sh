#!/bin/bash
set -eEuo pipefail
set -m
shopt -s lastpipe

################################################################################
# Configuration

source ./config.bash

################################################################################
# Implementation

[[ $EUID == 0 ]]
ulimit -v unlimited

################################################################################
# Clean-up

# Clean up mounts
function umount_if_mounted() {
	while grep -qF "$1" /proc/mounts
	do
		umount -v "$1"
	done
}

umount_if_mounted "$mountpoint"

# Clean up loop devices
losetup | \
	while read -r name _ _ _ _ file
	do
		if [[ "$file" == "$target_dir"/* || "$file" == /devs/* ]]
		then
			losetup -d "$name"
		fi
	done

# Unmount thincow
umount_if_mounted "$target_dir"

################################################################################
# Set up

if [[ -f "$ram_root"/did-setup ]]
then
	cold_start=false
else
	cold_start=true

	# # Clean up files
	# rm -vrf --one-file-system "$ram_root" #"$ssd_root" "$dev_root"
	# rm -vrf "$ssd_work"

	# #mkdir "$ram_root" #"$ssd_root" "$dev_root"
	# mkdir -vp "$checkpoint_dir" "$data_dir" "$metadata_dir" "$upstream_dir" "$target_dir"

	# n=0
	# for part in "${parts[@]}"
	# do
	# 	ln -s /dev/disk/by-partuuid/"$part" "$upstream_dir"/$((n++))-"$part"
	# done

	# # Restore snapshot
	# if [[ -e "$checkpoint_dir"/curr ]]
	# then
	# 	rm -vrf "$ssd_work"
	# 	clone "$checkpoint_dir"/curr/ssd-work "$ssd_work"
	# else
	# 	# Initial setup
	# 	:
	# fi

	touch "$ram_root"/did-setup
fi

function mount_thincow() {
	rm -f "$ram_root"/fifo
	mkfifo "$ram_root"/fifo
	cat "$ram_root"/fifo &
	fifo_pid=$!

	/home/vladimir/work/thincow/thincow_release \
		 --upstream="$upstream_dir" \
		 --data-dir="$data_dir" \
		 --metadata-dir="$metadata_dir" \
		 --max-block-map-size=$((96*1024*1024*1024)) \
		 --hash-table-size=$((1*1024*1024*1024)) \
		 "$target_dir" \
		 9> "$ram_root"/fifo

	# Try to prepopulate the null block in hash table
	for f in "$target_dir"/devs/*
	do
		size=$(stat -c %s "$f")
		for ((start=1 ; start < (size/1024/1024); start += 1024*1024))
		do
			printf 'Prepopulating: %s at %d/%d M\n' "$(basename "$f")" "$start" $((size/1024/1024))
			dd if="$f" of=/dev/null bs=1M count=1 skip="$start" status=none
		done
	done

	# Create loop devices
	loops=()
	if [[ ! -f "$ram_root"/work/did-remove-device ]]
	then
		n=0
		for part in "${parts[@]}"
		do
			loop=$(losetup --find --show "$target_dir"/devs/$n-"$part")
			n=$((n+1))
			loops+=("$loop")
		done
	fi
}
mount_thincow

exit

function unmount_thincow() {
	losetup -d "${loops[@]}"
	sleep 1
	umount -v "$ram_root"/target
	wait $fifo_pid
}

function checkpoint() {
	echo 'Checkpointing...'

	# local was_mounted=false
	# if grep -qF "$mountpoint" /proc/mounts
	# then
	# 	save_stats checkpoint
	# 	umount -v "$mountpoint"
	# 	was_mounted=true
	# fi
	# unmount_thincow
	fsfreeze -f "$mountpoint"
	sleep 1

	rm -vrf "$checkpoint_dir"/next
	mkdir "$checkpoint_dir"/next
	clone "$ssd_work" "$checkpoint_dir"/next/ssd-work

	rm -vrf "$checkpoint_dir"/last
	if [[ -e "$checkpoint_dir"/curr ]] ; then mv -v "$checkpoint_dir"/{curr,last} ; fi
	mv -v "$checkpoint_dir"/{next,curr}
	sync -f "$checkpoint_dir"
	# if [[ -e "$checkpoint_dir"/last ]] ; then rm -rvf "$checkpoint_dir"/last ; fi

	# mount_thincow
	# if $was_mounted
	# then
	# 	mount_fs
	# fi
	fsfreeze -u "$mountpoint"
}

function mount_fs() {
	# Create mount option string
	local mount_opts=rw
	for loop in "${loops[@]}"
	do
		mount_opts="$mount_opts,device=$loop"
	done
	mount -o "$mount_opts" "${loops[0]}" "$mountpoint"
}

################################################################################
# Action

mount_fs

if ! $cold_start
then
	checkpoint
fi

while true
do
	countdown 3600
	checkpoint
done

hdd_root=/mnt/2020-hdd-12t-raid/@data2020/archive2020
#ssd_root=/mnt/local/tmp/2020-06-23-move
#ram_root=/tmp/2020-08-24-move
ram_root=/mnt/local/tmp/2020-08-24-move

ram_work="$ram_root"/work
hdd_work="$hdd_root"/work

    data_dir="$hdd_work"/data
metadata_dir="$ram_work"/metadata
  status_dir="$ram_work"/status

  target_dir="$ram_root"/target
upstream_dir="$hdd_root"/upstream
checkpoint_dir="$hdd_root"/checkpoint

parts=(
	dest
	8866d95a-9022-4a64-b03e-faacb642cd8c # /dev/sdc1
	e804507f-324c-4e83-ba53-e0a5be7de701 # /dev/sde1
	19a92349-6170-4eb8-853e-4e1826617ab6 # /dev/sdg1
	a1f378aa-8c96-4ca5-b3ed-28a239d37b9b # /dev/sdj1
)

move_bs=$((1024 * 1024)) # 1M
move_step=$((1024 * 1024 * 1024)) # 1G

old_ram_root=/tmp/2020-06-23
old_target_dir=$old_ram_root/target
old_upstream_dir=$old_ram_root/upstream


function clone() {
	cp -v -a --reflink=always --sparse=auto "$@"
}

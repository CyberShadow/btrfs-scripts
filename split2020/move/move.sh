#!/bin/bash
set -eEuo pipefail
set -m
shopt -s lastpipe

# This script did a block-wise copy of the old thincow instance
# (located on my main PC hard drives) to a new one (located on the
# 12TB drives), which had the 4 original devices plus a new "final
# destination" device for the RAID5 array.

################################################################################
# Configuration

source ./config.bash

################################################################################
# Implementation

[[ $EUID == 0 ]]
ulimit -v unlimited

################################################################################
# Clean-up

# Clean up mounts
function umount_if_mounted() {
	while grep -qF "$1" /proc/mounts
	do
		umount -v "$1"
	done
}

# Unmount thincow
umount_if_mounted "$target_dir"

#exit

################################################################################
# Set up

if [[ ! -f "$status_dir"/did-setup ]]
then
	# Clean up files
	rm -vrf --one-file-system "$ram_root" #"$ssd_root" "$dev_root"
	rm -vrf "$hdd_work"

	#mkdir "$ram_root" #"$ssd_root" "$dev_root"
	mkdir -vp "$ram_work" "$checkpoint_dir" "$data_dir" "$metadata_dir" "$upstream_dir"

	# Restore snapshot
	if [[ -e "$checkpoint_dir"/curr ]]
	then
		tar xv -C "$ram_work" --zstd < "$checkpoint_dir"/curr/ram.tar.zst
		rm -vrf "$hdd_work"
		clone "$checkpoint_dir"/curr/hdd-work "$hdd_work"
	else
		# Initial setup

		n=0
		for part in "${parts[@]}"
		do
			size=$(stat -L -c %s "$old_target_dir"/devs/"$part")
			truncate -s "$size" "$upstream_dir"/$((n++))-"$part"
		done

		touch "$status_dir"/did-setup
	fi
fi

function mount_thincow() {
	rm -f "$ram_root"/fifo
	mkfifo "$ram_root"/fifo
	cat "$ram_root"/fifo &
	fifo_pid=$!

	mkdir -p "$target_dir"

	truncate --size $((256*1024*1024*1024)) "$metadata_dir"/blockmap

	/home/vladimir/work/thincow/thincow_release \
		 --upstream="$upstream_dir" \
		 --data-dir="$data_dir" \
		 --metadata-dir="$metadata_dir" \
		 --max-block-map-size=$((256*1024*1024*1024)) \
		 --max-cow-blocks=$((48*1024*1024*1024)) \
		 --hash-table-size=$((1*1024*1024*1024)) \
		 --retroactive \
		 "$target_dir" \
		 9> "$ram_root"/fifo

	# Try to prepopulate the null block in hash table
	for f in "$target_dir"/devs/*
	do
		size=$(stat -c %s "$f")
		for ((start=1 ; start < (size/1024/1024); start += 1024*1024))
		do
			printf 'Prepopulating: %s at %d/%d M\n' "$(basename "$f")" "$start" $((size/1024/1024))
			dd if="$f" of=/dev/null bs=1M count=1 skip="$start" status=none
		done
	done
}
mount_thincow

function unmount_thincow() {
	sleep 1
	umount -v "$target_dir"
	wait $fifo_pid
}

function checkpoint() {
	echo 'Checkpointing...'

	unmount_thincow

	rm -vrf "$checkpoint_dir"/next
	mkdir "$checkpoint_dir"/next
	tar cv -C "$ram_work" --sparse . | zstd -v -1 -T0 > "$checkpoint_dir"/next/ram.tar.zst
	clone "$hdd_work" "$checkpoint_dir"/next/hdd-work

	rm -vrf "$checkpoint_dir"/last
	if [[ -e "$checkpoint_dir"/curr ]] ; then mv -v "$checkpoint_dir"/{curr,last} ; fi
	mv -v "$checkpoint_dir"/{next,curr}
	sync -f "$checkpoint_dir"
	# if [[ -e "$checkpoint_dir"/last ]] ; then rm -rvf "$checkpoint_dir"/last ; fi

	mount_thincow
}

last_checkpoint=$(date +%s)
function maybe_checkpoint() {
	local now
	now=$(date +%s)
	local elapsed=$((now - last_checkpoint))
	if (( elapsed > 3600 ))
	then
		checkpoint
		last_checkpoint=$now
	fi
}

################################################################################
# Action

if [[ ! -f "$status_dir"/did-copy ]]
then
	mkdir -p "$status_dir"
	if [[ -f "$status_dir"/pos ]]
	then
		pos=$(cat "$status_dir"/pos)
	else
		echo 0 > "$status_dir"/pos
		pos=0
	fi

	while true
	do
		printf '========= %s / %s: At pos=%d =========\n' "$(date)" "$(date +%s)" "$pos"

		done=true

		dds=()
		trap 'kill "${dds[@]}" ; exit 1' INT
		columns=$(tput cols)

		n=0
		for part in "${parts[@]}"
		do
			source=$old_target_dir"/devs/"$part
			target="$target_dir"/devs/$((n++))-"$part"

			size=$(stat -c %s "$target")
			end=$(( (size + move_step - 1) / move_step ))
			printf '%s: %d/%d\n' "$part" $pos $end
			if (( pos >= end ))
			then
				continue
			fi

			step_blocks=$((move_step / move_bs))

			dd bs=$move_bs \
			   count=$step_blocks \
			   status=none \
			   if="$source" \
			   skip=$((pos * step_blocks)) \
			   iflag=fullblock \
				|
			pv -f -s "$move_step" -B "$move_step" -w $((columns-38)) -pterba 2> >(
				stdbuf -o0 tr '\r' '\n' |
					while IFS= read -r line
					do
						printf '\033[%dA%36s: %s\r\033[%dB' "$n" "$part" "$line" "$n" 1>&2
					done
			) \
				|
			dd bs=$move_bs \
			   count=$step_blocks \
			   status=none \
			   of="$target" \
			   seek=$((pos * step_blocks)) \
			   iflag=fullblock \
			   conv=notrunc \
			   &
			dds+=($!)
			done=false
		done

		for part in "${parts[@]}"
		do
			echo
		done

		if $done
		then
			break
		fi

		wait "${dds[@]}"
		trap '' INT

		pos=$(( pos + 1 ))
		echo "$pos" > "$status_dir"/pos

		maybe_checkpoint
	done

	touch "$status_dir"/did-copy
fi


checkpoint

# TODO, later, on the target machine:
# 1. Zero out the target device
# 2. Substitute the target device

#!/bin/bash
#set -eEuo pipefail

source ./config.bash

while true
do
	d=info-$(date +%s)
	mkdir "$d"
	btrfs-info "$mountpoint" > "$d"/btrfs-info.txt
	cat "$ram_root"/target/stats-full.txt > "$d"/stats-full.txt
	ps aux | grep '^.................................................D' |
		while read -r _ pid _ _ _ _ _ _ _ _ cmdline
		do
			cmdline=$(printf -- %s "$cmdline" | urlencode | sed 's/%20/+/g' | head -c 40)
			cat /proc/"$pid"/stack > "$d"/stack-"$pid"-"$cmdline".txt
		done
	chown -R vladimir: "$d"
	echo "$d" OK
	sleep 600
done

# Partition UUIDs (PARTUUID) containing filesystem to split
parts=(
	8866d95a-9022-4a64-b03e-faacb642cd8c # /dev/sdc1
	e804507f-324c-4e83-ba53-e0a5be7de701 # /dev/sde1
	19a92349-6170-4eb8-853e-4e1826617ab6 # /dev/sdg1
	a1f378aa-8c96-4ca5-b3ed-28a239d37b9b # /dev/sdj1
)
devids=(1 2 3 4)

resize_start=7424
resize_step=32
resize_unit=G

subvol_step=200
balance_step=8

# Where to place the temporary COW data.
stats_root=/mnt/data2016/tmp/2020-06-23-stats
checkpoint_root=/mnt/data2016/tmp/2020-06-23-snapshots
ram_root=/tmp/2020-06-23
ssd_root=/mnt/local/tmp/2020-06-23

new_uuid=$(uuidgen)

mountpoint="$ram_root"/mnt

dev_root=/dev/btrfs-split

#########################################################################################################
# btrfs-split-archive2016.sh config

# # Partition UUIDs (PARTUUID) containing filesystem to split
# parts=(
# 	8866d95a-9022-4a64-b03e-faacb642cd8c # /dev/sdc1
# 	e804507f-324c-4e83-ba53-e0a5be7de701 # /dev/sde1
# 	19a92349-6170-4eb8-853e-4e1826617ab6 # /dev/sdg1
# 	a1f378aa-8c96-4ca5-b3ed-28a239d37b9b # /dev/sdj1
# )
# devids=(1 2 3 4)

resize_start=7424
resize_step=32
resize_unit=G

# subvol_step=200
balance_step=1

# # Where to place the temporary COW data.
# checkpoint_root=/mnt/data2016/tmp/2020-06-23-snapshots
# ram_root=/tmp/2020-06-23
# ssd_root=/mnt/local/tmp/2020-06-23

# new_uuid=$(uuidgen)

# mountpoint="$ram_root"/mnt

# dev_root=/dev/btrfs-split

#########################################################################################################
# move config

hdd_root=/mnt/2020-hdd-12t-raid/@data2020/archive2020  # persistent
ssd_root=/mnt/local/tmp/2021-08-01-move                # ephemeral
ram_root=/tmp/2021-08-01-move                          # ephemeral

ssd_work="$ssd_root"/work
hdd_work="$hdd_root"/work

      data_dir="$hdd_work"/data
  metadata_dir="$ssd_work"/metadata
    status_dir="$ssd_work"/status
run_status_dir="$ram_root"

    target_dir="$ram_root"/target
  upstream_dir="$hdd_root"/upstream
checkpoint_dir="$hdd_root"/checkpoint
     stats_dir="$hdd_root"/stats

parts=(
	# 0-dest
	1-8866d95a-9022-4a64-b03e-faacb642cd8c # /dev/sdc1
	2-e804507f-324c-4e83-ba53-e0a5be7de701 # /dev/sde1
	3-19a92349-6170-4eb8-853e-4e1826617ab6 # /dev/sdg1
	4-a1f378aa-8c96-4ca5-b3ed-28a239d37b9b # /dev/sdj1
)
devids=(1 2 3 4)

mountpoint="$ram_root"/mnt
checkpoint_root="$hdd_root"/checkpoint

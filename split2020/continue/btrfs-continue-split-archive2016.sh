#!/bin/bash
set -eEuo pipefail
shopt -s lastpipe

# This script's goal is to continue moving the data off of the 4 8TB
# devices to the single RAID5-backed device.

# This script operates on the thincow devices populated by the script
# in ../move.

################################################################################
# Configuration

source ./config.bash

# - cleanup - clean-up and exit
# - resume - clean-up, unpack last snapshot, continue
# - continue - don't clean-up, don't unpack, continue as-is
mode=$1
case $mode in
	resume|continue|cleanup)
		;;
	*)
		printf 'Unknown mode: %s\n' "$mode" 1>&2
		exit 1
esac

################################################################################
# Functions

function clone() {
	cp -v -a -T --reflink=always --sparse=auto "$@"
}

function btrfs() {
	local btrfs_dir="$status_dir"/btrfs
	mkdir -p "$btrfs_dir"

	{
		printf -- '%q ' btrfs "$@"
		printf '\n'
	} 1>&2
	bwrap \
		--dev-bind / / \
		--bind "$btrfs_dir" /var/lib/btrfs \
		btrfs "$@"
}

function umount_if_mounted() {
	while grep -qF "$1" /proc/mounts
	do
		umount -v "$1"
	done
}

function mount_thincow() {
	rm -f "$ram_root"/fifo
	mkfifo "$ram_root"/fifo
	cat "$ram_root"/fifo &
	fifo_pid=$!

	truncate --size=$((256*1024*1024*1024)) "$metadata_dir"/blockmap

	mkdir -p "$target_dir"

	local opts
	opts=(
		 --upstream="$upstream_dir"
		 --data-dir="$data_dir"
		 --metadata-dir="$metadata_dir"
		 --max-block-map-size=$((256*1024*1024*1024))
		 --max-cow-blocks=$((48*1024*1024*1024))
		 --hash-table-size=$((1*1024*1024*1024))
		 --checksum-bits=8
		 "$target_dir"
	)

	printf '%q ' thincow "${opts[@]}"

	if true
	then
		# release
		/home/vladimir/work/thincow/thincow_release \
		 "${opts[@]}" \
		 9> "$ram_root"/fifo
	else
		# debug, in terminal
		cmd="$(printf '%q ' \
                      /home/vladimir/work/thincow/thincow \
                      -f \
		              "${opts[@]}") 9> $(printf %q "$ram_root"/fifo)"
		cmd=$cmd' ; echo "thincow exited with status $?" ; sleep infinity'
		urxvt -e sh -c "$cmd" &
		while [[ ! -e "$target_dir"/stats.txt ]]
		do
			echo 'Waiting for thincow (debug)...'
			sleep 1
		done
	fi

	# Try to prepopulate the null block in hash table
	for f in "$target_dir"/devs/*
	do
		size=$(stat -c %s "$f")
		for ((start=1 ; start < (size/1024/1024); start += 1024*1024))
		do
			printf 'Prepopulating: %s at %d/%d M\n' "$(basename "$f")" "$start" $((size/1024/1024))
			dd if="$f" of=/dev/null bs=1M count=1 skip="$start" status=none
		done
	done

	# Create loop devices
	local loop
	loops=()
	if [[ ! -f "$status_dir"/did-remove-device ]]
	then
		for part in "${parts[@]}"
		do
			loop=$(losetup --find --show "$ram_root"/target/devs/"$part")
			loops+=("$loop")
		done
	fi
	if true # [[ -f "$ram_root"/work/did-add-device ]]
	then
		dest_loop=$(losetup --find --show "$ram_root"/target/devs/0-dest)
	fi
}

function unmount_thincow() {
	losetup -d "${loops[@]}" "$dest_loop"
	sleep 1
	umount -v "$ram_root"/target
	wait $fifo_pid
}

function find_loop_for() {
	local part=$1

	losetup | \
		while read -r name _ _ _ _ file _
		do
			if [[ "$file" == /devs/"$part" || "$file" == "$target_dir"/devs/"$part" ]]
			then
				printf -- '%s\n' "$name"
				return
			fi
		done

	echo "Cannot find loop device for part $part" 1>&2
	exit 1
}

# Populate `loops` array and `dest_loop`.
# Used with "continue".
function detect_mounts() {
	if [[ ! -f "$status_dir"/did-remove-device ]]
	then
		local loop
		loops=()
		for part in "${parts[@]}"
		do
			loop=$(find_loop_for "$part")
			loops+=("$loop")
		done
	fi
	if true # [[ -f "$ram_root"/work/did-add-device ]]
	then
		dest_loop=$(find_loop_for 0-dest)
	fi
}

function mount_fs() {
	# Create mount option string
	local mount_opts=rw,noatime
	local loop
	for loop in "${loops[@]}" "$dest_loop"
	do
		mount_opts="$mount_opts,device=$loop"
	done
	mount -o "$mount_opts" "${loops[0]}" "$mountpoint"
}

function save_stats() {
	mkdir -p "$stats_dir"
	{
		if grep -qF "$mountpoint" /proc/mounts
		then
			btrfs device usage "$mountpoint"
			echo ---------------------------------------------
		fi
		time cat "$ram_root"/target/stats-full.txt
	} > "$stats_dir"/"$(date +%s)"-"$1".txt
}

function checkpoint() {
	last_checkpoint=$(date +%s)

	echo 'Checkpointing...'

	save_stats checkpoint

	local was_mounted=false
	if grep -qF "$mountpoint" /proc/mounts
	then
		umount -v "$mountpoint"
		was_mounted=true
	fi
	unmount_thincow

	rm -vrf "$checkpoint_dir"/next
	mkdir "$checkpoint_dir"/next
	tar cv -C "$ssd_work" --sparse . | zstd -v -1 -T0 > "$checkpoint_dir"/next/ram.tar.zst
	clone "$hdd_work" "$checkpoint_dir"/next/hdd-work

	rm -vrf "$checkpoint_dir"/last
	# if [[ -e "$checkpoint_dir"/curr ]] ; then mv -v "$checkpoint_dir"/{curr,last} ; fi
	if [[ -e "$checkpoint_dir"/curr ]] ; then mv -v "$checkpoint_dir"/curr "$checkpoint_dir"/"$(date +%s)" ; fi
	mv -v "$checkpoint_dir"/{next,curr}
	sync -f "$checkpoint_dir"
	# if [[ -e "$checkpoint_dir"/last ]] ; then rm -rvf "$checkpoint_dir"/last ; fi

	mount_thincow
	if $was_mounted
	then
		mount_fs
	fi
}

last_checkpoint=$(date +%s)
function maybe_checkpoint() {
	local now
	now=$(date +%s)
	local elapsed=$((now - last_checkpoint))
	if (( elapsed > 3600 ))
	then
		checkpoint
	fi
}

################################################################################
# Implementation

[[ $EUID == 0 ]]
ulimit -v unlimited

if [[ ! -f "$run_status_dir"/did-mount && "$mode" == continue ]]
then
	printf 'Setup was incomplete, cannot continue %s\n' "$mode" 1>&2
	exit 1
fi

################################################################################
# Clean-up

if [[ "$mode" != continue ]]
then
	# Clean up mounts
	umount_if_mounted "$mountpoint"

	# Clean up loop devices
	losetup | \
		while read -r name _ _ _ _ file _
		do
			if [[ "$file" == /devs/* || "$file" == "$target_dir"/devs/* ]]
			then
				losetup -d "$name"
			fi
		done

	# Unmount thincow
	umount_if_mounted "$target_dir"

	# Clean up files
	rm -rf --one-file-system "$ram_root" "$ssd_root" # "$dev_root"
	rm -vrf --one-file-system "$hdd_work"
fi

if [[ "$mode" == cleanup ]]
then
	exit
fi

################################################################################
# Set up

if [[ "$mode" == continue ]]
then
	# Restore variable state
	detect_mounts
else
	mkdir "$ram_root" "$ssd_root" # "$dev_root"
	mkdir -p "$checkpoint_root"

	# Restore snapshot
	mkdir "$ssd_root"/work
	if [[ -d "$checkpoint_root"/curr ]]
	then
		pv "$checkpoint_dir"/curr/ram.tar.zst | zstd -d | star xv -C "$ssd_work"
		clone "$checkpoint_dir"/curr/hdd-work "$hdd_work"
	fi

	mkdir -p "$ssd_root"/work/metadata

	mount_thincow

	mkdir "$mountpoint"

	# Mount
	mount_fs

	# Can continue from here.
	touch "$run_status_dir"/did-mount
fi

save_stats start

echo 'Syncing...'
env -C "$mountpoint" sh -c "btrfs subvolume sync ."

# Mount / unmount to ensure FS is clean
if [[ ! -f "$status_dir"/did-remount-"$(uname -r)" ]]
then
	umount -v "$mountpoint"
	mount_fs
	touch "$status_dir"/did-remount-"$(uname -r)"
	checkpoint
fi

if [[ ! -f "$status_dir"/did-scrub ]]
then
	if [[ ! -f "$status_dir"/did-scrub-start ]]
	then
		btrfs scrub start "$mountpoint"
		touch "$status_dir"/did-scrub-start
	else
		btrfs scrub status "$mountpoint" > "$ram_root"/scrub.txt
		status=$(awk '/^Status:/{print $2}' "$ram_root"/scrub.txt)
		if [[ "$status" == running ]]
		then
			echo "(scrub seems to already be running.)"
		else
			btrfs scrub resume "$mountpoint"
		fi
	fi
	btrfs scrub status "$mountpoint"

	while true
	do
		sleep 10
		btrfs scrub status "$mountpoint" > "$ram_root"/scrub.txt
		status=$(awk '/^Status:/{print $2}' "$ram_root"/scrub.txt)
		if [[ "$status" == finished ]]
		then
			break
		fi
		if [[ "$status" != running ]]
		then
			echo "Unknown btrfs-scrub status! $status"
			exit 1
		fi

		# maybe_checkpoint
		now=$(date +%s)
		elapsed=$((now - last_checkpoint))
		if (( elapsed > 3600 ))
		then
			btrfs scrub cancel "$mountpoint"
			checkpoint
			btrfs scrub resume "$mountpoint"
		fi
	done

	touch "$status_dir"/did-scrub
fi

if [[ ! -f "$status_dir"/current-size ]]
then
	echo $((resize_start + resize_step)) > "$status_dir"/current-size
fi

# Convert raid10 to single
for kind in s m d
do
	while true
	do
		btrfs balance start --force -${kind}convert=single,soft,limit=$balance_step "$mountpoint" | tee "$ram_root"/btrfs-balance-output.txt
		if ! grep -q '^Done, had to relocate [^0]' "$ram_root"/btrfs-balance-output.txt
		then
			break
		fi
		maybe_checkpoint
	done
done

# Incrementally (decrementally?) shrink old devices
while true
do
	size=$(cat "$status_dir"/current-size)
	size=$((size - resize_step))
	if [[ "$size" -le 0 ]]
	then
		break
	fi

	for devid in "${devids[@]}"
	do
		btrfs filesystem resize "$devid":"$size""$resize_unit" "$mountpoint"
	done
	echo "$size" > "$status_dir"/current-size
	checkpoint
done

# Remove old devices
if [[ ! -f "$status_dir"/did-remove-device ]]
then
	btrfs device remove "${loops[@]}" "$mountpoint"
	touch "$status_dir"/did-remove-device
	checkpoint
fi

# # Erase free space (to speed up network copy)
# ( dd if=/dev/zero bs=1M status=progress >> "$mountpoint"/zero ) || true

# rm -v "$mountpoint"/zero
# btrfs balance start -dusage=0 -musage=0 "$mountpoint"

# Unmount to flush
umount -v "$mountpoint"

checkpoint

# Cleanup
losetup -d "${loops[@]}" "$dest_loop"
# umount "$ram_root"/target

#!/bin/bash
set -eEuo pipefail
shopt -s lastpipe

# This script's goal is to allow mounting the thincow store, in read-only mode,
# on the target (archival) machine.

################################################################################
# Configuration

source ./config.bash

# - cleanup - clean-up (unmount) and exit
# - resume - clean-up, unpack mount
# - continue - try to continue mounting from where we left off
mode=$1
case $mode in
	resume|continue|cleanup)
		;;
	*)
		printf 'Unknown mode: %s\n' "$mode" 1>&2
		exit 1
esac

################################################################################
# Functions

function clone() {
	cp -v -a -T --reflink=always --sparse=auto "$@"
}

function btrfs() {
	local btrfs_dir="$status_dir"/btrfs
	mkdir -p "$btrfs_dir"

	{
		printf -- '%q ' btrfs "$@"
		printf '\n'
	} 1>&2
	bwrap \
		--dev-bind / / \
		--bind "$btrfs_dir" /var/lib/btrfs \
		btrfs "$@"
}

function umount_if_mounted() {
	while grep -qF "$1" /proc/mounts
	do
		umount -v "$1"
	done
}

function mount_thincow() {
	if [[ -e "$target_dir"/stats.txt ]]
	then
		printf '(thincow already seems to be mounted)\n' 1>&2
		detect_mounts
		return
	fi

	rm -f "$ram_root"/fifo
	mkfifo "$ram_root"/fifo
	cat "$ram_root"/fifo &
	fifo_pid=$!

	truncate --size=$((256*1024*1024*1024)) "$metadata_dir"/blockmap

	mkdir -p "$target_dir"

	local opts
	opts=(
		# /root/thincow_release_v0 # broken???
		/home/vladimir/work/thincow/thincow
		 --upstream="$upstream_dir"
		 --data-dir="$data_dir"
		 --metadata-dir="$metadata_dir"
		 --max-block-map-size=$((256*1024*1024*1024))
		 --max-cow-blocks=$((48*1024*1024*1024))
		 --hash-table-size=$((1*1024*1024*1024))
		 --read-only
		 "$target_dir"
	)

	printf -- '%q ' "${opts[@]}"

	"${opts[@]}" \
		9> "$ram_root"/fifo

	# Create loop devices
	local loop
	loops=()
	if [[ ! -f "$status_dir"/did-remove-device ]]
	then
		for part in "${parts[@]}"
		do
			loop=$(losetup --find --show "$ram_root"/target/devs/"$part")
			loops+=("$loop")
		done
	fi
	if true # [[ -f "$ram_root"/work/did-add-device ]]
	then
		dest_loop=$(losetup --find --show "$ram_root"/target/devs/0-dest)
	fi
}

function unmount_thincow() {
	losetup -d "${loops[@]}" "$dest_loop"
	unset loops dest_loop
	sleep 1
	umount -v "$ram_root"/target
	wait $fifo_pid
}

function find_loop_for() {
	local part=$1

	losetup | \
		while read -r name _ _ _ _ file _
		do
			if [[ "$file" == /devs/"$part" || "$file" == "$target_dir"/devs/"$part" ]]
			then
				printf -- '%s\n' "$name"
				return
			fi
		done

	echo "Cannot find loop device for part $part" 1>&2
	exit 1
}

# Populate `loops` array and `dest_loop`.
# Used with "continue".
function detect_loops() {
	if [[ ! -f "$status_dir"/did-remove-device ]]
	then
		local loop
		loops=()
		for part in "${parts[@]}"
		do
			loop=$(find_loop_for "$part")
			loops+=("$loop")
		done
	fi
	if true # [[ -f "$ram_root"/work/did-add-device ]]
	then
		dest_loop=$(find_loop_for 0-dest)
	fi
}

function mount_fs() {
	# Create mount option string
	local mount_opts=ro,norecovery,noatime
	local loop
	for loop in "${loops[@]}" "$dest_loop"
	do
		mount_opts="$mount_opts,device=$loop"
	done
	mount -o "$mount_opts" "${loops[0]}" "$mountpoint"
}

################################################################################
# Implementation

[[ $EUID == 0 ]]
ulimit -v unlimited

if [[ ! -f "$run_status_dir"/did-mount && "$mode" == continue ]]
then
	printf 'Setup was incomplete, cannot continue %s\n' "$mode" 1>&2
	exit 1
fi

################################################################################
# Clean-up

if [[ "$mode" != continue ]]
then
	# Clean up mounts
	umount_if_mounted "$mountpoint"

	# Clean up loop devices
	losetup | \
		while read -r name _ _ _ _ file _
		do
			if [[ "$file" == /devs/* || "$file" == "$target_dir"/devs/* ]]
			then
				losetup -d "$name"
			fi
		done

	# Unmount thincow
	umount_if_mounted "$target_dir"

	# Clean up files
	rm -rf --one-file-system "$ram_root" # "$ssd_root" # "$dev_root"
fi

if [[ "$mode" == cleanup ]]
then
	exit
fi

################################################################################
# Set up

if [[ "$mode" == continue ]]
then
	# Restore variable state
	detect_loops
else
	mkdir "$ram_root" # "$dev_root"
	mkdir -p "$ssd_root" "$checkpoint_root"

	# Restore snapshot
	if [[ ! -d "$ssd_work" ]]
	then
		mkdir "$ssd_work"
		if [[ -d "$checkpoint_root"/"$checkpoint" ]]
		then
			pv "$checkpoint_dir"/"$checkpoint"/ram.tar.zst | zstd -d | star xv -C "$ssd_work"
		fi
	fi

	mkdir -p "$ssd_root"/work/metadata

	mount_thincow

	mkdir "$mountpoint"

	# Mount
	mount_fs

	# Can continue from here.
	touch "$run_status_dir"/did-mount
fi

printf 'Filesystem mounted in %q\n' "$mountpoint"

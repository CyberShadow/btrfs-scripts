#!/bin/bash
set -eEuo pipefail
shopt -s lastpipe

################################################################################
# Configuration

source ./config.bash

################################################################################
# Implementation

[[ $EUID == 0 ]]
ulimit -v unlimited

# Resolve partition UUIDs to device names
declare -A part_devices
function scan_devices() {
	local part
	for part in "${parts[@]}"
	do
		# shellcheck disable=SC2024
		blkid -t PARTUUID="$part" > blkid.txt
		if [[ "$(wc -l < blkid.txt)" -ne 1 ]]
		then
			printf 'Found zero or more than one block device for PARTUUID %q!\n' "$part" 1>&2
			exit 1
		fi

		local dev
		dev="$(< blkid.txt cut -d : -f 1)"
		part_devices[$part]=$dev
	done
}
scan_devices

# Ensure source devices are read-only, to prevent accidentally writing to them.
for part in "${parts[@]}"
do
	# Make partition read only
	blockdev --setro "${part_devices[$part]}"
	# Make ATA read-only too, for good measure
	disk=$(printf %s "${part_devices[$part]}" | sed -n 's#^\(/dev/...\)[0-9]*$#\1#p')
	hdparm -r 1 "$disk"
done

################################################################################
# Clean-up

# Clean up DM
sudo dmsetup table | \
	while read -r device _
	do
		device=${device%:}
		if [[ "$device" == btrfs-split-* ]]
		then
			sudo dmsetup remove "$device"
		fi
	done

# Clean up mounts
function umount_if_mounted() {
	while grep -qF "$1" /proc/mounts
	do
		umount -v "$1"
	done
}
umount_if_mounted "$mountpoint"

# Clean up loop devices
losetup | \
	while read -r name _ _ _ _ file
	do
		if [[ "$file" == "$ram_root"/* || "$file" == /devs/* ]]
		then
			losetup -d "$name"
		fi
	done

# Unmount thincow
umount_if_mounted "$ram_root"/target

# Clean up files
rm -rf --one-file-system "$ram_root" "$ssd_root" "$dev_root"

#exit

################################################################################
# Set up

mkdir "$ram_root" "$ssd_root" "$dev_root"
mkdir -p "$checkpoint_root"

# Restore snapshot
mkdir "$ram_root"/work "$ssd_root"/work
if [[ -f "$checkpoint_root"/curr-ram.tar.zst ]]
then
	tar xv -C "$ram_root"/work --zstd < "$checkpoint_root"/curr-ram.tar.zst
	tar xv -C "$ssd_root"/work --zstd < "$checkpoint_root"/curr-ssd.tar.zst
fi

mkdir -p "$ram_root"/work/metadata

# Create upstream dir
mkdir "$ram_root"/{upstream,target}
for part in "${parts[@]}"
do
	ln -s /dev/disk/by-partuuid/"$part" "$ram_root"/upstream/"$part"
done
dd if=/dev/zero of="$ram_root"/upstream/dest bs=1 seek=9001720872960 count=0

function mount_thincow() {
	rm -f "$ram_root"/fifo
	mkfifo "$ram_root"/fifo
	cat "$ram_root"/fifo &
	fifo_pid=$!

	truncate --size=$((48*1024*1024*1024)) "$ram_root"/work/metadata/blockmap

	/home/vladimir/work/thincow/thincow_release \
		 --upstream="$ram_root"/upstream \
		 --data-dir="$ssd_root"/work/data \
		 --metadata-dir="$ram_root"/work/metadata \
		 --max-block-map-size=$((48*1024*1024*1024)) \
		 --max-cow-blocks=$((8*1024*1024*1024)) \
		 --hash-table-size=$((16*1024*1024*1024)) \
		 "$ram_root"/target \
		 9> "$ram_root"/fifo

	# Try to prepopulate the null block in hash table
	size=$(stat -c %s "$ram_root"/target/devs/dest)
	for ((start=1 ; start < (size/1024/1024); start += 1024*1024))
	do
		printf 'Prepopulating: at %d/%d M\n' "$start" $((size/1024/1024))
		dd if="$ram_root"/target/devs/dest of=/dev/null bs=1M count=1 skip="$start"
	done

	# Create loop devices
	loops=()
	if [[ ! -f "$ram_root"/work/did-remove-device ]]
	then
		for part in "${parts[@]}"
		do
			loop=$(losetup --find --show "$ram_root"/target/devs/"$part")
			loops+=("$loop")
		done
	fi
	if [[ -f "$ram_root"/work/did-add-device ]]
	then
		loop=$(losetup --find --show "$ram_root"/target/devs/dest)
		loops+=("$loop")
	fi
}
mount_thincow

function unmount_thincow() {
	losetup -d "${loops[@]}"
	sleep 1
	umount -v "$ram_root"/target
	wait $fifo_pid
}

# Create fake /dev and /proc/partitions for libblkid
cp -av /dev/{null,zero,full} "$dev_root"/
(
	printf 'major minor  #blocks  name\n\n'
	while read -r major minor sz name
	do
		is_target=false
		for loop in "${loops[@]}"
		do
			if [[ "$loop" == /dev/"$name" ]]
			then
				is_target=true
			fi
		done

		if $is_target
		then
			printf ' %s %s %s %s\n' "$major" "$minor" "$sz" "$name"
			cp -av /dev/"$name" "$dev_root"/
		fi
	done < /proc/partitions
) > partitions.txt

function mount_fs() {
	# Create mount option string
	local mount_opts=rw
	for loop in "${loops[@]}"
	do
		mount_opts="$mount_opts,device=$loop"
	done
	mount -o "$mount_opts" "${loops[0]}" "$mountpoint"
}

function save_stats() {
	{
		btrfs device usage "$mountpoint"
		echo ---------------------------------------------
		cat "$ram_root"/target/stats-full.txt
	} > "$stats_root"/"$(date +%s)"-"$1".txt
}

function checkpoint() {
	echo 'Checkpointing...'

	local was_mounted=false
	if grep -qF "$mountpoint" /proc/mounts
	then
		save_stats checkpoint
		umount -v "$mountpoint"
		was_mounted=true
	fi
	unmount_thincow

	tar cv -C "$ram_root"/work --sparse . | zstd -v -1 -T0 > "$checkpoint_root"/next-ram.tar.zst
	tar cv -C "$ssd_root"/work --sparse . | zstd -v -1 -T0 > "$checkpoint_root"/next-ssd.tar.zst
	if [[ -f "$checkpoint_root"/curr-ram.tar.zst ]] ; then mv -v "$checkpoint_root"/{curr,last}-ram.tar.zst ; fi
	if [[ -f "$checkpoint_root"/curr-ssd.tar.zst ]] ; then mv -v "$checkpoint_root"/{curr,last}-ssd.tar.zst ; fi
	mv -v "$checkpoint_root"/{next,curr}-ram.tar.zst
	mv -v "$checkpoint_root"/{next,curr}-ssd.tar.zst
	# if [[ -f "$checkpoint_root"/last-ram.tar.zst ]] ; then rm -v "$checkpoint_root"/last-ram.tar.zst ; fi
	# if [[ -f "$checkpoint_root"/last-ssd.tar.zst ]] ; then rm -v "$checkpoint_root"/last-ssd.tar.zst ; fi

	mount_thincow
	if $was_mounted
	then
		mount_fs
	fi
}

mkdir "$mountpoint"

# Mount / unmount to ensure FS is clean
if [[ ! -f "$ram_root"/work/did-remount ]]
then
	mount_fs
	umount "$mountpoint"
	touch "$ram_root"/work/did-remount
	checkpoint
fi

# Run btrfstune with fake /proc/partitions
if [[ ! -f "$ram_root"/work/did-btrfstune ]]
then
	args=(
		time
		sudo
		bwrap
		--dev-bind / /
		--dev-bind "$dev_root" /dev
		--bind partitions.txt /proc/partitions
		btrfstune
		#-U "$new_uuid" # or -M (faster, Linux >= 5.0 only)
		-M "$new_uuid"
		"${loops[0]}"
	) ; "${args[@]}"
	touch "$ram_root"/work/did-btrfstune
	checkpoint
fi

# Mount
mount_fs

save_stats start

echo 'Syncing...'
env -C "$mountpoint" sh -c "btrfs subvolume sync ."

# Delete everything irrelevant
items=(
	# 'home/@arch*'
	# 'home/@home*'
	# 'home/@note4*'
	# 'home/@ubuntu*'
	# 'home/@win*'
	'k3/@data*'
	'k3/@home*'
	'k3/@ram*'
	'k3/@root*'
	# '@windata'
	'@archive2016'
)

for item in "${items[@]}"
do
	env -C "$mountpoint" find -mount -type f -path ./"$item" -delete

	while true
	do
		( env -C "$mountpoint" find -mount -type d -path ./"$item" || true ) | head -n "$subvol_step" | mapfile -t subvols
		if [[ "${#subvols[@]}" -eq 0 ]]
		then
			break
		fi
		env -C "$mountpoint" btrfs subvolume delete -c "${subvols[@]}"
		env -C "$mountpoint" btrfs subvolume sync .
		checkpoint
	done
done

# Add new device
if [[ ! -f "$ram_root"/work/did-add-device ]]
then
	dest_loop=$(losetup --find --show "$ram_root"/target/devs/dest)
	loops+=("$dest_loop")
	btrfs device add "$dest_loop" "$mountpoint"
	touch "$ram_root"/work/did-add-device
	checkpoint
fi

# Change profile to single
btrfs balance start --force -dconvert=single,limit=0 -mconvert=single,limit=0 "$mountpoint"

if [[ ! -f "$ram_root"/work/current-size ]]
then
	echo $((resize_start + resize_step)) > "$ram_root"/work/current-size
fi

if false # Will finish this after move
then
	# Convert raid10 to single
	for kind in s m d
	do
		while true
		do
			btrfs balance start --force -${kind}convert=single,soft,limit=$balance_step "$mountpoint" | tee "$ram_root"/btrfs-balance-output.txt
			if ! grep -q '^Done, had to relocate [^0]' "$ram_root"/btrfs-balance-output.txt
			then
				break
			fi
			checkpoint
		done
	done

	# Incrementally (decrementally?) shrink old devices
	while true
	do
		size=$(cat "$ram_root"/work/current-size)
		size=$((size - resize_step))
		if [[ "$size" -le 0 ]]
		then
			break
		fi

		for devid in "${devids[@]}"
		do
			btrfs filesystem resize "$devid":"$size""$resize_unit" "$mountpoint"
		done
		echo "$size" > "$ram_root"/work/current-size
		checkpoint
	done

	# Remove old devices
	if [[ ! -f "$ram_root"/work/did-remove-device ]]
	then
		btrfs device remove "${loops[@]}" "$mountpoint"
		touch "$ram_root"/work/did-remove-device
		checkpoint
	fi
fi

# Erase free space (to speed up network copy)
( dd if=/dev/zero bs=1M status=progress >> "$mountpoint"/zero ) || true

rm -v "$mountpoint"/zero
btrfs balance start -dusage=0 -musage=0 "$mountpoint"

# Unmount to flush
umount "$mountpoint"

checkpoint

# Cleanup
losetup -d "${loops[@]}" "$dest_loop"
# umount "$ram_root"/target
